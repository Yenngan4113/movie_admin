import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { userService } from "../../services/userServices";

let initialState = {
  user: null,
};

export const setUserLoginActionServ = createAsyncThunk(
  "userSlice/login",
  async (dataLogin) => {
    let result = await userService.postDangNhap(dataLogin);
    return result.data.content;
  }
);
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserLogin: (state, { payload }) => {
      state.user = payload;
    },
  },
  extraReducers: {
    // pending
    [setUserLoginActionServ.pending]: (state, action) => {
      state.loading = true;
    },
    // Success
    [setUserLoginActionServ.fulfilled]: (state, action) => {
      state.loading = false;
      state.user = action.payload;
    },
    // Reject
    [setUserLoginActionServ.rejected]: (state, action) => {},
  },
});

export default userSlice.reducer;
export const { setUserLogin } = userSlice.actions;
