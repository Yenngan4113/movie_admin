import { configureStore } from "@reduxjs/toolkit";
import demoSlice from "./demoSlice";
import userSlice from "./userSlice";

export const store = configureStore({
  reducer: { demoSlice, userSlice },
});
