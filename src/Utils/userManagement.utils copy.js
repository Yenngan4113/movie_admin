import { Button, message, Tag } from "antd";
import { userService } from "../services/userServices";

const handleEdit = (data) => {
  console.log(data);
};
export const headerTableUser = [
  {
    title: "Account",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
    render: (text) => <span className="text-blue-500">{text}</span>,
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
    // render: (text) => <a>{text}</a>,
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    // render: (text) => <a>{text}</a>,
  },
  {
    title: "Số điện thoại",
    dataIndex: "soDT",
    key: "soDT",
    // render: (text) => <a>{text}</a>,
  },
  {
    title: "Loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (text) => {
      return (
        <span>
          {text == "KhachHang" ? (
            <Tag color="purple">Khách hàng</Tag>
          ) : (
            <Tag color="orange">Quản Trị</Tag>
          )}
        </span>
      );
    },
  },
  {
    title: "Hành động",
    dataIndex: "action",
    key: "action",
    render: ({ onDelete, onEdit }, record) => {
      return (
        <>
          <Button type="primary" className=" bg-blue-600">
            Sửa
          </Button>
          <Button
            type="primary"
            danger
            onClick={() => {
              userService
                .deleteUser(record.taiKhoan)
                .then((res) => {
                  onDelete();
                  message.success("Xoá thành công");
                  console.log(res);
                })
                .catch((err) => {
                  message.error(err.response.data.content);
                  console.log(err);
                });
            }}
          >
            Xóa
          </Button>
        </>
      );
    },
  },
];
