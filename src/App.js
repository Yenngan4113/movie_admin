import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import UserManagePage from "./pages/UserManagePage/UserManagePage";
import DemoToolkitPage from "./pages/DemoToolkitPage/DemoToolkitPage";
import LoginPage from "./pages/LoginPage/LoginPage";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<UserManagePage />} />
          <Route path="/toolkit" element={<DemoToolkitPage />} />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
