import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { giamSoLuong, tangSoLuong } from "../../Redux/slices/demoSlice";

export default function DemoToolkitPage() {
  let number = useSelector((state) => {
    return state.demoSlice.number;
  });
  let dispatch = useDispatch();
  return (
    <div className="container mx-auto 9y-40">
      <button
        className="px-5 py-2 rounded bg-red-500 text-white"
        onClick={() => {
          dispatch(giamSoLuong(10));
        }}
      >
        Giam
      </button>
      <span>Number - {number}</span>
      <button
        className="px-5 py-2 rounded bg-green-500 text-white"
        onClick={() => {
          dispatch(tangSoLuong(10));
        }}
      >
        Tang
      </button>
    </div>
  );
}
