import { Button, Checkbox, Form, Input } from "antd";
import { userService } from "../../services/userServices";
import { useNavigate } from "react-router-dom";

import {
  setUserLogin,
  setUserLoginActionServ,
} from "../../Redux/slices/userSlice";
import { useDispatch } from "react-redux";
import { localStorageServ } from "../../services/localServices";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    // userService
    //   .postDangNhap(values)
    //   .then((res) => {
    //     dispatch(setUserLogin(res.data.content));
    //     navigate("/");
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    dispatch(setUserLoginActionServ(values))
      // unwrap sẽ biến hàm thành 1 promise
      .unwrap()
      .then((res) => {
        console.log(res);
        localStorageServ.setUserInfor(res);
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
      });
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="matKhau"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item name="remember" valuePropName="checked">
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="bg-blue-800">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default LoginPage;
