import { Space, Table, Tag } from "antd";
import { headerTableUser } from "../../../Utils/userManagement.utils";

const TableUsers = ({ ListUser }) => (
  <Table columns={headerTableUser} dataSource={ListUser} />
);

export default TableUsers;
