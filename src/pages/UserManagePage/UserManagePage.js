import React, { useEffect, useState } from "react";
import { userService } from "../../services/userServices";
import { Carousel, message } from "antd";
import TableUsers from "./TableUsers/TableUsers";
import { useWindowSize } from "../../Hook/useWindowSize";
import HeaderResponsive from "../../Component/Header/HeaderResponsive";

export default function UserManagePage() {
  let windowSize = useWindowSize();
  console.log(windowSize);
  const [userList, setUserList] = useState([]);
  let fetchUserList = () => {
    userService
      .getUserList()
      .then((res) => {
        let dataRaw = res.data.content.map((user) => {
          return {
            ...user,
            action: {
              onDelete: () => {
                userService
                  .deleteUser(user.taiKhoan)
                  .then((res) => {
                    message.success("Xoá thành công");
                    fetchUserList();
                  })
                  .catch((err) => {
                    message.error(err.response.data.content);
                  });
              },
              onEdit: () => {
                console.log("yes");
              },
            },
          };
        });
        setUserList(dataRaw);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    fetchUserList();
  }, []);
  return (
    <>
      <header className="text-red-500">
        <HeaderResponsive />
      </header>
      <div className="container mx-auto">
        <TableUsers ListUser={userList} />
      </div>
    </>
  );
}
