import axios from "axios";
import { BASE_URL, instance, TOKEN_CYBERSOFT } from "./configURL";

export const userService = {
  postDangNhap: (dataLogin) => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
    //   method: "POST",
    //   data: dataLogin,
    //   headers: { TokenCybersoft: TOKEN_CYBERSOFT },
    // });
    return instance.post("/api/QuanLyNguoiDung/DangNhap", dataLogin);
  },
  getUserList: () => {
    return instance.get("/api/QuanLyNguoiDung/LayDanhSachNguoiDung");
  },
  deleteUser: (taikhoan) => {
    return instance.delete(
      `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taikhoan}`
    );
  },
};
