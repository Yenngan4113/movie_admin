import axios from "axios";
import { localStorageServ } from "./localServices";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwMyIsIkhldEhhblN0cmluZyI6IjAxLzAxLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3MjUzMTIwMDAwMCIsIm5iZiI6MTY0NzUzNjQwMCwiZXhwIjoxNjcyNjc4ODAwfQ.v1pky9yKwnujpoxePbaS26rxq_cGpKrk0GvA0sHAVqY";

// Create axios Instance
export const instance = axios.create({
  baseURL: BASE_URL,
  timeout: 1000,
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    // Khi dữ liệu là null hoặc undefined thì bị lỗi, để dấu ? để nếu là 2 giá trị thì sẽ k chạy code
    Authorization:
      "Bearer " + `${localStorageServ.getUserInfor()?.accessToken}`,
  },
});
